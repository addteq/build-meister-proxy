var uuidv4 = require('uuid/v4');
var configfile = './config.json';
var CONFIG = require(configfile);
var fs = require('file-system');

CONFIG.AuthKey = uuidv4();
fs.writeFile(configfile, JSON.stringify(CONFIG), function (err) {
    if (err){
        console.log(err);
        console.log('Unable to set AuthKey in JSON file');
    }
    console.log('AuthKey set in configFile');
});
