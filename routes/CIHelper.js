var CONFIG = require('.././config.json');

// URL , Password and Username
var CIURL = CONFIG.bamboo.url + CONFIG.bamboo.restPath;
var username = CONFIG.bamboo.user;
var password = CONFIG.bamboo.pwd;
var ENDPOINTProject = '/project/';
var ENDPOINTPlan = '/plan/';
var ENDPOINTResult = '/result/'
var trailingPoints = '?expand=results.result';
var ENDPOINTqueue = '/queue/'

var cheerio = require('cheerio');

var Promise = require('promise');
var rp = require('request-promise');

// Authorization code and REST END Points
var Authorization = new Buffer(username + ':' + password).toString('base64');

function CIServiceHelper() {};

CIServiceHelper.prototype.getCIBuildInfo = function(callbackfn, planKeyChoice){

    return new Promise(function (resolve, reject) {
        if( CONFIG.bamboo.projectKey === '' ){
            resolve({
                'message' : 'Please set the bamboo project key in the node proxy service',
                'session' : false,
                'status'  : 200
            });
        }

        if( (CONFIG.bamboo.planKey === '') && (typeof planKeyChoice === 'undefined') ){

            getProjectPlans(CONFIG.bamboo.projectKey).then(function(resp){
                console.log(resp);

                if( resp.plans.size === 0 ){
                    resolve({
                        'message' : 'There are no plans defined in the Project '+ resp.name ,
                        'session' : false,
                        'status'  : 200
                    });
                }else if( resp.plans.size === 1 ){
                    // Get build information
                    callbackfn(resp.key , resp.plans.plan[0].key,resolve,reject);
                }else{
                    // request to choose the plan from the project
                    console.log("Choose from optios");
                    //var message = 'Please choose one of the following Plan to get the information from <break time="150ms"/>'
                    var message = new Array();
                    var count = 1;
                    resp.plans.plan.forEach(function(val){
                        if( count <= 5 ){
                            message.push({
                                'name' : val.shortName,
                                'planKey' : val.planKey.key
                            });
                            //message += count + ' <break time="150ms"/> ' + val.name + ' <break time="150ms"/> ';
                        }
                        count ++;
                    });

                    resolve({
                        'message' : message,
                        'session' : true,
                        'status'  : 200
                    });
                }
            }).catch(function(err){
                // console.log(err);
                var retmessage = {};
                if( err.message.indexOf('Basic Authentication Failure') !== -1 ){
                    retmessage.message = 'Authentication Failed with CI tool';
                    retmessage.status = 401;
                }else{
                    retmessage.message = err.error.message;
                    retmessage.status = err.statusCode;
                }
                retmessage.session = false;
                reject (retmessage);
            })
        }else{
            console.log("plan and project found keey found");
            var plankey = CONFIG.bamboo.planKey;
            if(CONFIG.bamboo.planKey === '' ){
                plankey = planKeyChoice;
            }
            callbackfn(CONFIG.bamboo.projectKey , plankey,resolve,reject);
        }
    });
}


CIServiceHelper.prototype.getLastBuildStatus = function(project,plan,rs,rj){
    console.log( " in get last build " + project + plan);
    console.log(CIURL + ENDPOINTResult + plan + trailingPoints);

    var options = {
        method: 'GET',
        uri: CIURL + ENDPOINTResult + plan + trailingPoints,
        headers: {
            "Content-Type": "application/json",
            "Authorization" : " Basic "+ Authorization
        },
        json: true
    };

    rp(options).then(function(respcallback){
        console.log( respcallback );

        var $ = cheerio.load('<div>'+ respcallback.results.result[0].buildReason +'</div>');

        rs({
            'message' : 'Last build ' + respcallback.results.result[0].buildResultKey + ' and was ' +  respcallback.results.result[0].buildState +
                ' was trigger because of  ' + $('div').text().replace(/<[^>]*>/g,"")
                + ' on ' + respcallback.results.result[0].prettyBuildStartedTime,
            'session' : false,
            'status'  : 200
        });
    }).catch(function(err ){
        console.log(err.message);

        rj({
            'message' : err.error.message,
            'session' : false,
            'status'  : err.statusCode
        });
    });
}


CIServiceHelper.prototype.getLastSuccessfulBuild = function(project,plan,rs,rj){
    console.log( " in get last build " + project + plan);
    console.log(CIURL + ENDPOINTResult + plan + trailingPoints);

    var options = {
        method: 'GET',
        uri: CIURL + ENDPOINTResult + plan + trailingPoints,
        headers: {
            "Content-Type": "application/json",
            "Authorization" : " Basic "+ Authorization
        },
        json: true
    };

    rp(options).then(function(respcallback){
        console.log( respcallback );
        var message = '';

        respcallback.results.result.forEach(function(val){
            if( val.buildState === 'Successful' && message === '' ){
                var $ = cheerio.load('<div>'+ val.buildReason +'</div>');

                message = 'Last Successful build was ' + val.buildResultKey + ' was trigger because of  ' + $('div').text().replace(/<[^>]*>/g,"")
                 + ' on ' + val.prettyBuildStartedTime;
            }
        });

        if(message === ''){
            message = 'There are no Successful builds in the last 25 build in Plan ' + respcallback.results.result[0].plan.key
        }

        rs({
            'message' : message ,
            'session' : false,
            'status'  : 200
        });
    }).catch(function(err ){
        console.log(err.message);

        rj({
            'message' : err.error.message,
            'session' : false,
            'status'  : err.statusCode
        });
    });
}


CIServiceHelper.prototype.getLastFailedBuild = function(project,plan,rs,rj){
    console.log( " in get last build " + project + plan);
    console.log(CIURL + ENDPOINTResult + plan + trailingPoints);

    var options = {
        method: 'GET',
        uri: CIURL + ENDPOINTResult + plan + trailingPoints,
        headers: {
            "Content-Type": "application/json",
            "Authorization" : " Basic "+ Authorization
        },
        json: true
    };

    rp(options).then(function(respcallback){
        console.log( respcallback );
        var message = '';

        respcallback.results.result.forEach(function(val){
            if( val.buildState === 'Failed' && message === '' ){
                var $ = cheerio.load('<div>'+ val.buildReason +'</div>');

                message = 'Last Failed build was ' + val.buildResultKey + ' was trigger because of ' + $('div').text().replace(/<[^>]*>/g,"")
                 + ' on ' + val.prettyBuildStartedTime;
            }
        })

        if(message === ''){
            message = 'There are no Failed builds in the last 25 build in Plan ' + respcallback.results.result[0].plan.key
        }

        rs({
            'message' : message ,
            'session' : false,
            'status'  : 200
        });
    }).catch(function(err ){
        console.log(err.message);

        rj({
            'message' : err.error.message,
            'session' : false,
            'status'  : err.statusCode
        });
    });
}


CIServiceHelper.prototype.triggerNewBuild = function(project,plan,rs,rj){
    console.log( " in get trigger build " + project + plan);
    console.log(CIURL + ENDPOINTqueue + plan + '.json');

    var options = {
        method: 'POST',
        uri: CIURL + ENDPOINTqueue + plan + '.json',
        headers: {
            "Content-Type": "application/json",
            "Authorization" : " Basic "+ Authorization
        },
        json: true
    };

    rp(options).then(function(respcallback){
        console.log( respcallback );
        var message = 'New manual build ' + respcallback.buildNumber + ' has been triggered on build plan ' + respcallback.buildResultKey ;       

        rs({
            'message' : message ,
            'session' : false,
            'status'  : 200
        });
    }).catch(function(err ){
        console.log(err.response.body.message);
        //console.log(err.response.body['status-code']);

        rj({
            'message' : err.response.body.message,
            'session' : false,
            'status'  : err.response.body['status-code']
        });
    });
}


function getProjectPlans(project){
    console.log("inside getting project plan ");

    var options = {
        method: 'GET',
        uri: CIURL + ENDPOINTProject + project + '.json?expand=plans',
        headers: {
            "Content-Type": "application/json",
            "Authorization" : " Basic "+ Authorization
        },
        json: true
    };
    return rp(options);
}

module.exports = CIServiceHelper;
