var sconfigFile = '.././config.json';
var dconfigFile = './config.json';

var CONFIG = require(sconfigFile);
var fs = require('file-system');

var CIEndHelperObject = require('./CIHelper');

var appRouter = function(app) {

    //set the authkey provide during account linking
    // Authkey must be part of incoming request body with header as content type  => application/x-www-form-urlencoded
    // app.post("/setAuthKey", function(req, res) {
    //     console.log( req.body.AuthKey );
    // 
    //     if(! req.body.AuthKey ){
    //         res.status(400).send({'message' : 'Missing AuthKey in request parameters'});
    //     }else{
    //         console.log(req.body.AuthKey);
    //         CONFIG.AuthKey = req.body.AuthKey;
    // 
    //         fs.writeFile(dconfigFile, JSON.stringify(CONFIG), function (err) {
    //             if (err){
    //                 console.log(err);
    //                 res.send(500).send({'message' : 'Unable to set AuthKey in JSON file'});
    //             }
    //             res.status(200).send({'message' : 'AuthKey set in configFile'});
    //         });
    //     }
    //     return res;
    // });


    // reponse strcture give to alexa query lambda skill
    /*
    {
        message : 'message to be read by Alexa'
        status  : the http reponse status code
        session : if the session with alexa is active and intractable
    }

    planKeyChoice - the key choosen by user based on the options provided
    */

    // get last  biuld status from bamboo
    app.post("/lastbuildstatus", function(req, res) {

        if(! req.headers.authorization ){
            res.status(400).send({'message': 'No Authorization  present in request' });
        }else{
            if ( CONFIG.AuthKey !== req.headers.authorization.replace('Basic ','') ){
                res.status(400).send({'message': 'AuthKey does not match' });
            }else{
                // Make call to bamboo to get the required info
                var CIHelper = new CIEndHelperObject();
                console.log("Plan Choice " + req.body.planChoice );

                 CIHelper.getCIBuildInfo(CIHelper.getLastBuildStatus , req.body.planChoice ).then(function(resps){
                    console.log(resps)
                    res.status(resps.status).send(resps);
                }).catch(function(err){
                    console.log(err);
                    res.status(err.status).send(err);
                })

                // res.status(200).send({'message': 'Build was successful' });
                // res.status(500).send({'message': 'Cannot find build information' });
            }
        }
        return res;
    });



    // get last successful biuld from bamboo
    app.post("/lastsuccessfulbuild", function(req, res) {

        if(! req.headers.authorization ){
            res.status(400).send({'message': 'No Authorization  present in request' });
        }else{
            if ( CONFIG.AuthKey !== req.headers.authorization.replace('Basic ','') ){
                res.status(400).send({'message': 'AuthKey does not match' });
            }else{
                // Make call to bamboo to get the required info
                var CIHelper = new CIEndHelperObject();
                console.log("Plan Choice " + req.body.planChoice );

                 CIHelper.getCIBuildInfo(CIHelper.getLastSuccessfulBuild , req.body.planChoice ).then(function(resps){
                    console.log(resps)
                    res.status(resps.status).send(resps);
                }).catch(function(err){
                    console.log(err);
                    res.status(err.status).send(err);
                })

                // res.status(200).send({'message': 'Build was successful' });
                // res.status(500).send({'message': 'Cannot find build information' });
            }
        }
        return res;
    });


    // get last failed biuld from bamboo
    app.post("/lastfailedbuild", function(req, res) {

        if(! req.headers.authorization ){
            res.status(400).send({'message': 'No Authorization  present in request' });
        }else{
            if ( CONFIG.AuthKey !== req.headers.authorization.replace('Basic ','') ){
                res.status(400).send({'message': 'AuthKey does not match' });
            }else{
                // Make call to bamboo to get the required info
                var CIHelper = new CIEndHelperObject();
                console.log("Plan Choice " + req.body.planChoice );

                 CIHelper.getCIBuildInfo(CIHelper.getLastFailedBuild , req.body.planChoice ).then(function(resps){
                    console.log(resps)
                    res.status(resps.status).send(resps);
                }).catch(function(err){
                    console.log(err);
                    res.status(err.status).send(err);
                })

                // res.status(200).send({'message': 'Build was successful' });
                // res.status(500).send({'message': 'Cannot find build information' });
            }
        }
        return res;
    });


    // trigger new build from build plan
    app.post("/triggerNewBuild", function(req, res) {

        if(! req.headers.authorization ){
            res.status(400).send({'message': 'No Authorization  present in request' });
        }else{
            if ( CONFIG.AuthKey !== req.headers.authorization.replace('Basic ','') ){
                res.status(400).send({'message': 'AuthKey does not match' });
            }else{
                // Make call to bamboo to get the required info
                var CIHelper = new CIEndHelperObject();
                console.log("Plan Choice " + req.body.planChoice );

                 CIHelper.getCIBuildInfo(CIHelper.triggerNewBuild , req.body.planChoice ).then(function(resps){
                    console.log(resps)
                    res.status(resps.status).send(resps);
                }).catch(function(err){
                    console.log(err);
                    res.status(err.status).send(err);
                })

                // res.status(200).send({'message': 'Build was successful' });
                // res.status(500).send({'message': 'Cannot find build information' });
            }
        }
        return res;
    });


    app.get("/", function(req, res) {
        res.status(200).send({'status' : 'live'});
    });
}

module.exports = appRouter;
