# CI proxy service

This node module acts as a proxy service for Alexa CI skill in order to retrieve information from the bamboo server.

Make sure to set up the proxy service before enabling the CI skill in Alexa Echo App.

To set up the node proxy service, provide the following information in the config file.

### bamboo server info

```javascript
{
    "bamboo":{
        "url":"<provide the url of the bamboo server, example - https://bamboo.company.com>",
        "restPath":"<provide the rest url path for the bamboo server -  /rest/api/latest>",
        "user":"<Username>",
        "pwd":"<password>",
        "projectKey":"<The project in the bamboo server on which Alexa will be quering information about",
        "planKey":"<Provide optional plan key to associated the skill to query a particular plan in the project.  If the plan key is not provided, alexa would request user to choose among the plan available within the project on every call"
        },
    "AuthKey":"Create an Auth key, this will be shared with Alexa skill to uniquely identify incoming calls coming from Alexa skill. "
}
```

### node proxy service setup

Make sure to install node version 4 or above

Install all node modules
```javascript
npm install
```

The Authkey can be generated and set in the config file  either by running 
```javascript
node generateAuthkey.js
```
or user can create custom key and set the value in config file. 

** Note this Auth key will have to provided as part of account linking when enabling the skill in alexa echo app



Run the proxy service  
```javascript
node server.js
```

by default the service runs on port 3000. Expose the post as a webservice using any webserver of your choice.

To test you can use ngrok. Install ngrok and expose port 3000
```javascript
ngrok http 3000
```

Provide the url to the node proxy service as part of account linking when enabling skill in Alexa Echo App



# Installing the Alexa skill

From the Alexa Skill store, enable the Build Meister skill

![Install Alexa Skill](images/buildmeister.png  "Install Alexa Skill")


Enable the skill and perform the account linking by providing the URL to the node proxy service and AuthKey 

![Setup the buildmeister skill](images/accountlinking.png  "Setup the buildmeister skill")

once the skill is activated in the amazon echo app. Request the information from Alexa echo using the following phrases


## Alexa Build Meister Interactions


## Sample Interaction Phrases

**Get Last Build Status**
This query retrieves the last build status from the project key provided with in the CI Proxy service. If the build plan is not specified in the config file then Alexa would provide user with options to choose one of the build Plan

If build plan is set in CI proxy service
```
User : Alexa , Ask Buildmeister , to get the last build status.
Alexa :  <Provide information on the last build>
```

If not set
```
User : Alexa , Ask Buildmeister , to get the last build status.
Alexa: Choose one of the following Build Plan to get the information from.
        1 . Integration Build
        2 . QA Build

User : Number One
Alexa :  <Provide information on the last build with in the requested plan>
```


**Get Last Successful Build**
This query retrieves the last Successful build from the project key provided with in the CI Proxy service. If the build plan is not specified in the config file then Alexa would provide user with options to choose one of the build Plan

If build plan is set in CI proxy service
```
User : Alexa , Ask Buildmeister , what is the last Successful build.
Alexa :  <Provide information on the last Successful build>
```

If not set
```
User : Alexa , Ask Buildmeister , to get the last Successful build.
Alexa: Choose one of the following Build Plan to get the information from.
        1 . Integration Build
        2 . QA Build

User : Number One
Alexa :  <Provide information on the last Successful build with in the requested plan>
```


Other Skills included

```
**Last failed Build** - Get the last failed build in the plan
**Trigger New Build** - Trigger a manual build on the Plan
```





